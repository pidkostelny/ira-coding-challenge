import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';
import { SidenavComponent } from './sidenav/sidenav.component';
import { AccountComponent } from './account.component';
import { BeneficiariesComponent } from './beneficiaries/beneficiaries.component';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [SidenavComponent, AccountComponent, BeneficiariesComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AccountRoutingModule,
  ]
})
export class AccountModule { }
