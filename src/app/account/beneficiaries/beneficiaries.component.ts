import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {animate, query, sequence, style, transition, trigger} from '@angular/animations';

const ONE_TO_HUNDRED_REGEXP = /^[1-9][0-9]?$|^100$/;
const DATE_REGEXP = /^(((0)[0-9])|((1)[0-2]))(\/)([0-2][0-9]|(3)[0-1])(\/)\d{4}$/;

@Component({
  selector: 'app-beneficiaries',
  templateUrl: './beneficiaries.component.html',
  styleUrls: ['./beneficiaries.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':leave', [
        style({transform: 'translateX(0)', height: '*', opacity: 1}),
        sequence([
          animate('500ms ease-in', style({transform: 'translateX(200%)', opacity: 0})),
          animate('200ms ease-in-out', style({height: '0'})),
        ])
      ]),
      transition(':enter', [
        style({height: '0', opacity: 0}),
        sequence([
          animate('500ms ease-out', style({height: '*', opacity: 0.5})),
          animate('500ms ease-out', style({opacity: 1}))
        ])
      ])
    ])
  ]
})
export class BeneficiariesComponent implements OnInit {

  beneficiaries: FormGroup[] = [];

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.addBeneficiaries();
  }

  removeBeneficiary(group: FormGroup) {
    if (group.get('checked').value) {
      const i = this.beneficiaries.indexOf(group);
      this.beneficiaries.splice(i, 1);
    } else if (this.beneficiaries.length === 1 || !group.valid || !group.get('checked')) {
      group.reset({
        type: '1',
        relation: 'default'
      });
      return;
    } else {
      const i = this.beneficiaries.indexOf(group);
      this.beneficiaries.splice(i, 1);
    }

    if (this.emptyFieldNotExists()) {
      this.addBeneficiaries();
    }
  }

  submitBeneficiary(group: FormGroup) {
    if (group.invalid) {
      alert('please fill correctly all fields');
      return;
    }

    group.get('checked').setValue(true);

    if (this.checkTotal()) {
      this.addBeneficiaries();
    }
  }

  addBeneficiaries() {
    const group = this.fb.group({
      name: ['', [Validators.required]],
      birth: ['', [Validators.required, Validators.pattern(DATE_REGEXP)]],
      type: ['1'],
      typeValue: [''],
      relation: ['default', Validators.required],
      trust: ['', [Validators.required, Validators.pattern(ONE_TO_HUNDRED_REGEXP)]],
      checked: [false]
    });
    this.beneficiaries.push(group);
  }

  checkTotal() {
    if (this.total > 100) {
      alert('Total sum is more than 100');
      return false;
    } else if (this.total === 100) {
      return false;
    }
    return true;
  }

  emptyFieldNotExists(): boolean {
    return this.beneficiaries.reduce((p, c) => p && c.get('checked').value, true);
  }

  onEnter(group: FormGroup) {
    group.markAllAsTouched();
    this.submitBeneficiary(group);
  }

  onDelete(group: FormGroup) {
    this.removeBeneficiary(group);
  }

  get total() {
    let sum = 0;
    this.beneficiaries.forEach(v => {
      const value = +v.get('trust').value;
      sum += v.get('checked').value && !isNaN(value) ? value : 0;
    });
    return sum;
  }

}
